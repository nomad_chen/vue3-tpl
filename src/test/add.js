define([
  'require',
  'dependency'
], function(require, factory) {
  'use strict';
  const add = (a, b) => a + b;
  return add;
});