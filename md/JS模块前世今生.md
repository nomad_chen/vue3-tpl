### AMD
实现规范库require.js
特点：依赖前置，异步加载
```
// 定义模块 引用math.js, 命名math2
define(['math'], function (){
　　var add = function (x,y){
　　　　return x+y;
　　};
　　return {
　　　　add: add,
       add2: math.add(x, y)
　　};
});

// 引用模块
require(['math2'], function(math) {
  alert(math.add(1,1));
  alert(math.add2(1,1));
})
```
### CMD
实现规范库sea.js
特点：依赖就近，按需加载
```
// 定义模块  myModule.js
define(function(require, exports, module) {
  var $ = require('jquery.js')
  $('div').addClass('active');
});

// 加载模块
seajs.use(['myModule.js'], function(my){

});
```
### CommonJS
模块的加载实质上就是，注入exports、require、module三个全局变量，然后执行模块的源码，然后将模块的 exports 变量的值输出。
```
//add.js
const add = (a, b) => a + b
module.exports = add
//index.js
const add = require('./add')
add(1, 5)
```
### ESM